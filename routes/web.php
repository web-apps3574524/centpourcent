<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
});
/*
Route::get('/mentionsLegales', function () {
    return view('mentionsLegales');
});
Route::get('/politiqueDeConfidentialite', function () {
    return view('politiqueDeConfidentialite');
});
Route::get('/contact', function () {
    return view('contact');
});
 */
