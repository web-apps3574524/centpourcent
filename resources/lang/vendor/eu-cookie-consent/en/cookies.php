<?php

return [
    'acceptAllButton' => 'Accepter',
    'save' => 'Refuser',
    'PopupTitle' => 'Ce site utilise des cookies !',
    'PopupDescription' => 'Pour votre confort et pour optimiser votre utilisation, nous utilisons des cookies.',

    'essential' => 'Données collectées',
    'essential_description' => 'Nous utilisons deux types de cookies, un cookie de session et un jeton CSRF. Rien d\'autre… Aucune autre donnée n\'est utilisé par 100pourcent.net. Ce site ne vous traque pas, ne vends pas vos données, ne contient pas de publicité.',

    'session' => 'Cookie de Session : Le serveur utilise un cookie de session pour identifier la session utilisateur.',
    'xsrf-token' => 'Cookie de jeton-XSRF : Le serveur génère un cookie de jeton-XSRF afin de vérifier qu\'un utilisateur enregistré est bien à l\'origine des requêtes passées à l\'application.',
];
