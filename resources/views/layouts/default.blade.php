<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
        <link rel="stylesheet" href="{{asset('css/fonts.css')}}">
        <title>{{ config('app.name') }}</title>
        <!-- Matomo -->
        <script>
          var _paq = window._paq = window._paq || [];
          /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
          _paq.push(['trackPageView']);
          _paq.push(['enableLinkTracking']);
          (function() {
            var u="//matomo.framend.com/";
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
          })();
        </script>
        <!-- End Matomo Code -->
        <noscript>
        <!-- Matomo Image Tracker-->
        <img referrerpolicy="no-referrer-when-downgrade" src="https://analytics.framend.com/matomo.php?idsite=1&amp;rec=1" style="border:0" alt="" />
        <!-- End Matomo -->
        </noscript>
        <!-- Cookieconsent  -->
    </head>
    <body>
    @yield('navbar')
    <p></p>
    <video width="72%" controls autoplay muted>
      <source src="video/centpourcentPresentation.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
    </video>
    @yield('actualOffers')
    @yield('futureOffers')
    @yield('pastOffers')
        <!-- footer -->
        <div class="footer-love-final">
            <div class="contactText">
            <p>7 Bvd Félix Mercader 66000 Perpignan - 0699255704</p>
            <div class="bottomMenu">
                <a href="https://localhost:8000/mentionsLegales/">
                    <div class="buttonMenLeg buttonBottomMenu">
                        <p>Mentions Légales</p>
                    </div>
                </a>
                <a href="https://localhost:8000/politiqueDeConfidentialité/">
                    <div class="buttonPolConf buttonBottomMenu">
                        <p>Politique de confidentialité</p>
                    </div>
                </a>
                <a href="https://localhost:8000/contact/">
                    <div class="buttonContact buttonBottomMenu">
                        <p>Contact</p>
                    </div>
                </a>
            </div>
            <div class="copyrightYear">
                <p>Tous droits réservés </p><?php echo date("Y");?></p> © </p>{{ config('app.name') }}
            </div>
            <div class="footer-love-text">
                Créé avec Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
            </div>
        </div>
    </body>
</html>
