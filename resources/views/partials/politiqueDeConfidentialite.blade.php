    <div class="privacyPolicy">
        <h2>Politique de confidentialité</h2>
        <p>Cette politique de confidentialité du domaine 100pourcent.net complète les mentions légales que vous pouvez consulter à l’adresse suivante : Mentions légales.</p>
        <p>De manière générale, la visite du site 100pourcent.net se fait sans avoir à décliner votre identité ou à fournir des informations personnelles vous concernant. Le site ne gère pas ce type d'information et n'exploite donc pas des informations vous concernant. Cependant le site 100pourcent.net utilise la technologie des cookies pour analyser son audience, améliorer son site Web et afficher un contenu publicitaire personnalisé.</p>
        <p>Lors de la consultation du site 100pourcent.net, des informations sont susceptibles d’être enregistrées dans des fichiers appelés « Cookies ». Ces fichiers sont enregistrés dans votre ordinateur, tablette ou téléphone mobile. Cette page permet de comprendre ce qu’est un Cookie, à quoi ils servent et comment on peut les paramétrer.
        </p>
        <h3>Qu’est-ce qu’un Cookie ?</h3>
        <p>Il s’agit d’un fichier texte déposé dans un espace dédié du disque dur de votre ordinateur, tablette, téléphone mobile ou tout autre appareil. Ce Cookie peut être lu que par son émetteur. Il ne vous identifie pas personnellement, mais permet d’identifier votre terminal dans lequel il est enregistré, pendant une durée de validité limitée.</p>
        <p>À quoi servent les Cookies ?</p>
        <p>Quatre types de Cookies, peuvent être enregistrés dans votre terminal à l’occasion d’une visite de notre site :</p>
        <p>1. Les Cookies techniques</p>
        <p>Les Cookies techniques permettent de signaler votre passage sur telle ou telle page et ainsi d’améliorer votre confort de navigation.</p>
        <p>2. Les Cookies de performance</p>
        <p>Ils sont émis par nos prestataires techniques aux fins de mesurer l’audience des différents contenus et rubriques de notre site, afin de les évaluer et de mieux les organiser. Ces Cookies permettent également, le cas échéant, de détecter des problèmes de navigation et par conséquent d’améliorer l’ergonomie de nos services. Ces Cookies ne produisent que des statistiques anonymes et des volumes de fréquentation.</p>
    </div>
