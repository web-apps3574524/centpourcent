    <div class="contactForm">
        <h3>1. éditeur</h3>
        <p>
            Raison sociale :
            Domiciliation :
            RCS :
            Directeur de la publication :
        </p>
        <h3>2. hébergement</h3>
        <p>
            Raison sociale :
            Domiciliation :
            RCS :
            Directeur de la publication :
        </p>
        <h3>3. Données personnelles</h3>
        <p>
        Conformément à la loi Informatique et Libertés du 6 Janvier 1978 modifiée par la Loi du 6 août 2004, vous disposez d’un droit d’accès, de rectification, de modification et de suppression des données personnelles que vous nous avez communiquées. Vous pouvez exercer ce droit à tout moment en nous contactant par l'intermédiaire de notre page contact.

        Si vous êtes abonné à notre newsletter, vous pouvez demander à ne plus recevoir ces courriers électroniques soit en suivant les instructions figurant en fin de chacun de ces courriers lorsque vous les recevez, soit en nous contactant par l'intermédiaire de notre page contact.

        D'une façon générale, vous pouvez visiter notre site Internet sans avoir à décliner votre identité et à fournir des informations personnelles vous concernant.
        </p>
        <h3>4. Cookies</h3>
        <p>
        100pourcent.net utilise des cookies pour différentes raisons. Cela nous permet d'adapter nos intéractions comme par exemple vous proposer l'inscription à notre newsletter si il s'agit de votre première visite.

        Cette notification vise à vous informer de l’utilisation que nous faisons de telles technologies. Elle explique les types de technologie dont nous nous servons et leurs utilités.
        </p>
        <h3>4.1 Qu'est-ce qu'un cookie ?</h3>
        <p>
        Les cookies sont de petits fichiers de données qui sont stockés sur votre appareil lorsque vous visitez et utilisez des sites Web et des services en ligne. Ils sont utilisés couramment pour faire fonctionner les sites Web, pour en optimiser le fonctionnement, pour fournir des renseignements ou bien encore pour personnaliser les prestations et la publicité.
        </p>
        <h3>4.2 Pourquoi 100pourcent.net utilise des cookies ?</h3>
        <p>
        Pour le bon fonctionnement du site, 100pourcent.net utilise plusieurs types de cookies :
        </p>
        <p>
            Cookies internes : il s'agit des cookies qui nous permettent d'adapter le comportement du site en fonction du client. Par exemple proposer l'adhésion à la newsletter si il s'agit d'une première visite...
            Cookies d'analyses : ces derniers sont crées par nos outils d'analyse d'audience et permettent de mieux connaître le comportement de nos lecteurs : pages visitées, rebonds...
            Cookies publicitaires : ces cookies exploitent des informations concernant votre visite sur ce site et sur d’autres sites Web, telles que les pages que vous consultez, l’utilisation que vous faites de notre service. Ils permettent à notre régie publicitaire de proposer des publicités plus pertinentes, sur notre site Web ou ailleurs. Notamment en limitant l’affichage de certaines publicités auxquelles vous auriez déjà été exposées. Les publicités sont qualifiées de “publicités ciblées par centres d’intérêt”. De nombreux cookies publicitaires associés à notre service appartiennent à nos fournisseurs de services.
        </p>
        <p>À savoir que nous ne conservons aucune information permettant de vous identifier personnellement dans les données de nos cookies.
        </p>
        <p>
        De manière générale, pour en savoir plus sur les cookies, nous vous invitons à consulter la page de la CNIL : http://www.cnil.fr/vos-libertes/vos-traces/les-cookies/
        </p>
        <h3>5. Propriété intellectuelle</h3>
        <p>
        Le site 100pourcent.net appartient et est exploité par la société <société>. Les textes et photos présents sur le site, sont protégés par les lois en vigueur sur la propriété intellectuelle. A ce titre, ils ne peuvent être copiés, reproduits, modifiés, réédités, dénaturés, transmits ou distribués de quelque manière que ce soit, sous quelque support que ce soit, de façon partielle ou intégrale, sans l’autorisation écrite et préalable de la société <société>.
        </p>
        <h3>6. Dispositions juridiques</h3>
        <p>
        Le site 100pourcent.net et son contenu sont régis par le Droit Français. Tout litige éventuel s’y rapportant sera soumis à la compétence des tribunaux français.
        </p>
    </div>
