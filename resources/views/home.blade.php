@extends('layouts.default')
@section('navbar')
    @include('partials.navbar')
@endsection
@section('actualOffers')
    @include ('partials.actualOffers')
@endsection
@section('futureOffers')
    @include ('partials.futureOffers')
@endsection
@section('pastOffers')
    @include ('partials.pastOffers')
@endsection
